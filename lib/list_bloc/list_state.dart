part of 'list_bloc.dart';


class ListState {
    final List<TreeStructure> notes;

  const ListState({this.notes = const []});

  ListState copyWith({List<TreeStructure>? notes}) {
    return ListState(
      notes: notes ?? this.notes,
    );
  }
}

