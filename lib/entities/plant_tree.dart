import 'package:hive/hive.dart';

part 'plant_tree.g.dart';


@HiveType(typeId: 1)
class BranchStructure {
  @HiveField(0)
  String? title;
  @HiveField(1)
  String? description;
  @HiveField(2)
  String? imagePath;
}

@HiveType(typeId: 2)
class TreeStructure {
  @HiveField(0)
  BranchStructure domen = BranchStructure();

  @HiveField(1)
  BranchStructure kingdom = BranchStructure();

  @HiveField(2)
  BranchStructure clade = BranchStructure();

  @HiveField(3)
  BranchStructure order = BranchStructure();

  @HiveField(4)
  BranchStructure family = BranchStructure();

  @HiveField(5)
  BranchStructure species = BranchStructure();
}

