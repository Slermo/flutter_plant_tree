// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'plant_tree.dart';

// **************************************************************************
// TypeAdapterGenerator
// **************************************************************************

class BranchStructureAdapter extends TypeAdapter<BranchStructure> {
  @override
  final int typeId = 1;

  @override
  BranchStructure read(BinaryReader reader) {
    final numOfFields = reader.readByte();
    final fields = <int, dynamic>{
      for (int i = 0; i < numOfFields; i++) reader.readByte(): reader.read(),
    };
    return BranchStructure()
      ..title = fields[0] as String?
      ..description = fields[1] as String?
      ..imagePath = fields[2] as String?;
  }

  @override
  void write(BinaryWriter writer, BranchStructure obj) {
    writer
      ..writeByte(3)
      ..writeByte(0)
      ..write(obj.title)
      ..writeByte(1)
      ..write(obj.description)
      ..writeByte(2)
      ..write(obj.imagePath);
  }

  @override
  int get hashCode => typeId.hashCode;

  @override
  bool operator ==(Object other) =>
      identical(this, other) ||
      other is BranchStructureAdapter &&
          runtimeType == other.runtimeType &&
          typeId == other.typeId;
}

class TreeStructureAdapter extends TypeAdapter<TreeStructure> {
  @override
  final int typeId = 2;

  @override
  TreeStructure read(BinaryReader reader) {
    final numOfFields = reader.readByte();
    final fields = <int, dynamic>{
      for (int i = 0; i < numOfFields; i++) reader.readByte(): reader.read(),
    };
    return TreeStructure()
      ..domen = fields[0] as BranchStructure
      ..kingdom = fields[1] as BranchStructure
      ..clade = fields[2] as BranchStructure
      ..order = fields[3] as BranchStructure
      ..family = fields[4] as BranchStructure
      ..species = fields[5] as BranchStructure;
  }

  @override
  void write(BinaryWriter writer, TreeStructure obj) {
    writer
      ..writeByte(6)
      ..writeByte(0)
      ..write(obj.domen)
      ..writeByte(1)
      ..write(obj.kingdom)
      ..writeByte(2)
      ..write(obj.clade)
      ..writeByte(3)
      ..write(obj.order)
      ..writeByte(4)
      ..write(obj.family)
      ..writeByte(5)
      ..write(obj.species);
  }

  @override
  int get hashCode => typeId.hashCode;

  @override
  bool operator ==(Object other) =>
      identical(this, other) ||
      other is TreeStructureAdapter &&
          runtimeType == other.runtimeType &&
          typeId == other.typeId;
}
