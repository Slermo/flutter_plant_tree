import 'package:flutter_lab4/entities/plant_tree.dart';
import 'package:flutter_lab4/features/hive_functions.dart';

class HiveController {

  HiveController._();

  /// the one and only instance of this singleton
  static final instance = HiveController._();

  Future<void> initHive() async {
    await HiveDataBase.instance.initHive();
  }

  Future<void> addItemIntoHive(TreeStructure element) async {
    //TreeStructure element = TreeStructure();
    await HiveDataBase.instance.addItem(element);
  }

  Future<void> deleteAllItemsFromHive() async {
    await HiveDataBase.instance.deleteAllItems();
  }
  Future<void> deleteItemFromHive(int element) async {
    await HiveDataBase.instance.deleteItem(element);
  }

  Future<List<TreeStructure>> getAllItemsFromHive() async {
    return await HiveDataBase.instance.getItems();
  }
}
