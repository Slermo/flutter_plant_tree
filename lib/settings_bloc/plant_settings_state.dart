part of 'plant_settings_bloc.dart';


class PlantSettingsState {
  final TreeStructure? tree;

  const PlantSettingsState({this.tree});

  PlantSettingsState copyWith({TreeStructure? tree}) {
    return PlantSettingsState(
      tree: tree ?? this.tree,
    );
  }
}
