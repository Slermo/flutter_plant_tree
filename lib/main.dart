import 'package:flutter/material.dart';
import 'features/hive_controller.dart';
import 'home_page.dart';

void main() async{
  WidgetsFlutterBinding.ensureInitialized(); 
  await HiveController.instance.initHive();
  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({super.key});

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      title: 'Flutter Demo',
      theme: ThemeData(
        primarySwatch: Colors.blue,
      ),
      home: const MyHomePage(),
    );
  }
}

