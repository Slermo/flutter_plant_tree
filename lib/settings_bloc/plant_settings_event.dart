part of 'plant_settings_bloc.dart';

@immutable
abstract class PlantSettingsEvent {}

class UpdateTree extends PlantSettingsEvent {
  final TreeStructure tree;

  UpdateTree({required this.tree});
}
