import 'dart:async';
import 'package:bloc/bloc.dart';
import 'package:flutter_lab4/features/hive_controller.dart';
import 'package:meta/meta.dart';

import '../entities/plant_tree.dart';

part 'plant_settings_event.dart';
part 'plant_settings_state.dart';

class PlantSettingsBloc extends Bloc<PlantSettingsEvent, PlantSettingsState> {
  PlantSettingsBloc() : super(const PlantSettingsState()) {
       on<UpdateTree>(_onUpdatingTree);
  }

  FutureOr<void> _onUpdatingTree(UpdateTree event, Emitter<PlantSettingsState> emit) {
    HiveController.instance.addItemIntoHive(event.tree);
  }
}
