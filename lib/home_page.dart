import 'package:flutter/material.dart';
import 'package:flutter_lab4/settings_bloc/plant_settings.dart';

import 'list_bloc/listpage.dart';

class MyHomePage extends StatelessWidget {
  const MyHomePage({super.key});

  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: Scaffold(
        backgroundColor: const Color.fromARGB(255, 95, 141, 78),
        body: Container(
          decoration: const BoxDecoration(
            gradient: LinearGradient(
              begin: Alignment.topRight,
              end: Alignment.bottomLeft,
              colors: [
                Color.fromARGB(255, 95, 141, 78),
                Color.fromARGB(255, 229, 217, 182),
              ],
            ),
          ),
          child: Center(
              child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              Padding(
                padding: const EdgeInsets.all(8.0),
                child: RichText(
                  text: const TextSpan(
                      text: "Leaf",
                      style: TextStyle(
                          color: Color.fromARGB(255, 221, 228, 213),
                          fontSize: 40),
                      children: <TextSpan>[
                        TextSpan(
                          text: '- plants encyclopedia',
                          style: TextStyle(
                              color: Color.fromARGB(255, 188, 226, 145),
                              fontSize: 20),
                        ),
                      ]),
                ),
              ),
              Padding(
                padding: const EdgeInsets.fromLTRB(40, 8, 40, 8),
                child: GestureDetector(
                  onTap: () {
                    Navigator.push(
                      context,
                      MaterialPageRoute(
                          builder: (context) => const ListPage()),
                    );
                  },
                  child: Container(
                    decoration: BoxDecoration(
                      color: const Color.fromARGB(255, 164, 190, 123),
                      border: Border.all(
                        width: 2,
                        color: Colors.white10,
                      ),
                      borderRadius: BorderRadius.circular(12),
                    ),
                    child: Center(
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.center,
                        children: const [
                          Padding(
                            padding: EdgeInsets.all(8.0),
                            child: Icon(
                              Icons.list,
                              color: Color.fromARGB(255, 55, 75, 48),
                            ),
                          ),
                          Text(
                            "Посмотреть список",
                            style: TextStyle(
                              fontSize: 22.0,
                              color: Color.fromARGB(255, 55, 75, 48),
                            ),
                          ),
                        ],
                      ),
                    ),
                  ),
                ),
              ),
              Padding(
                padding: const EdgeInsets.fromLTRB(40, 8, 40, 8),
                child: GestureDetector(
                  onTap: () {
                    Navigator.push(
                      context,
                      MaterialPageRoute(
                          builder: (context) => PlantSettingsPage()),
                    );
                  },
                  child: Container(
                    decoration: BoxDecoration(
                      color: const Color.fromARGB(255, 164, 190, 123),
                      border: Border.all(
                        width: 2,
                        color: Colors.white10,
                      ),
                      borderRadius: BorderRadius.circular(12),
                    ),
                    child: Center(
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.center,
                        children: const [
                          Padding(
                            padding: EdgeInsets.all(8.0),
                            child: Icon(
                              Icons.add_card,
                              color: Color.fromARGB(255, 55, 75, 48),
                            ),
                          ),
                          Text(
                            "Добавить элемент",
                            style: TextStyle(
                              fontSize: 22.0,
                              color: Color.fromARGB(255, 55, 75, 48),
                            ),
                          ),
                        ],
                      ),
                    ),
                  ),
                ),
              ),
            ],
          )),
        ),
      ),
    );
  }
}
