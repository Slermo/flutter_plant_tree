part of 'list_bloc.dart';

@immutable
abstract class ListEvent {}

class DeleteLinks extends ListEvent {
    final int num;

  DeleteLinks({required this.num});
}

class UpdateLinks extends ListEvent {}
