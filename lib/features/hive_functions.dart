import 'dart:io';
import 'package:hive_flutter/hive_flutter.dart';
import '../entities/plant_tree.dart';
import 'package:path_provider/path_provider.dart';

class HiveDataBase {
  HiveDataBase._() {}
  static final instance = HiveDataBase._();

  late Box<TreeStructure> _box;

  Future<void> initHive() async {
    Directory dir = await getApplicationDocumentsDirectory();
    await Hive.initFlutter(dir.path);
    if (!Hive.isAdapterRegistered(0)) {
      Hive.registerAdapter<TreeStructure>(TreeStructureAdapter());
      Hive.registerAdapter<BranchStructure>(BranchStructureAdapter());
    }
    _box = await Hive.openBox<TreeStructure>('tree_note');
  }

  Future<void> addItem(TreeStructure element) async {
    await _box.add(element);
    print("added new item.");
  }

  Future<void> deleteItem(int element) async {
      await _box.deleteAt(element);
    
  }

  Future<void> deleteAllItems() async {
    await _box.deleteFromDisk();
    _box = await Hive.openBox<TreeStructure>('tree_note');
    print("delete all hive elements.");
  }

  Future<List<TreeStructure>> getItems() async {
    return _box.values.toList();
  }
}
