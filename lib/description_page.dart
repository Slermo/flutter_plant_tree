import 'package:flutter/gestures.dart';
import 'package:flutter/material.dart';
import 'package:flutter/src/widgets/container.dart';
import 'package:flutter/src/widgets/framework.dart';
import 'package:flutter_simple_treeview/flutter_simple_treeview.dart';

import 'entities/plant_tree.dart';

class ShowDescriptionTree extends StatefulWidget {
  final TreeStructure curtree;

  const ShowDescriptionTree({super.key, required this.curtree});

  @override
  State<ShowDescriptionTree> createState() => _ShowDescriptionTreeState();
}

class _ShowDescriptionTreeState extends State<ShowDescriptionTree> {
  bool isImageExist = false;
  String? imagePath = "";
  bool isDescriptionExist = false;
  String? description = "";

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Container(
        decoration: const BoxDecoration(
          gradient: LinearGradient(
            begin: Alignment.topRight,
            end: Alignment.bottomLeft,
            colors: [
              Color.fromARGB(255, 95, 141, 78),
              Color.fromARGB(255, 229, 217, 182),
            ],
          ),
        ),
        child: Column(
          children: [
            Expanded(
              flex: 1,
              child: Padding(
                padding: const EdgeInsets.all(8.0),
                child: GestureDetector(
                  onTap: () {
                    Navigator.pop(context);
                  },
                  child: Container(
                    decoration: BoxDecoration(
                      color: const Color.fromARGB(255, 204, 112, 75),
                      border: Border.all(
                        width: 2,
                        color: Colors.white10,
                      ),
                      borderRadius: BorderRadius.circular(12),
                    ),
                    child: const Center(
                      child: Text(
                        "Назад",
                        style: TextStyle(
                          fontSize: 22.0,
                        ),
                      ),
                    ),
                  ),
                ),
              ),
            ),
            Expanded(
              flex: 3,
              child: TreeView(
                nodes: [
                  TreeNode(
                    content: RichText(
                      text: TextSpan(
                        text: '- Домен: ',
                        style: const TextStyle(
                          color: Color.fromARGB(255, 39, 22, 0),
                          fontSize: 22.0,
                        ),
                        children: [
                          TextSpan(
                            text: widget.curtree.domen.title!,
                            style: const TextStyle(
                              decoration: TextDecoration.underline,
                              color: Color.fromARGB(255, 39, 22, 0),
                              fontSize: 22.0,
                            ),
                            recognizer: TapGestureRecognizer()
                              ..onTap = () {
                                setState(
                                  () {
                                    imagePath = widget.curtree.domen.imagePath;
                                    if (imagePath != null) {
                                      isImageExist = true;
                                    } else {
                                      isImageExist = false;
                                    }
                                    description =
                                        widget.curtree.domen.description;
                                    if (description != null) {
                                      isDescriptionExist = true;
                                    } else {
                                      isDescriptionExist = false;
                                    }
                                  },
                                );
                              },
                          ),
                        ],
                      ),
                    ),
                  ),
                  TreeNode(
                    content: RichText(
                      text: TextSpan(
                        text: '- Царство: ',
                        style: const TextStyle(
                          color: Color.fromARGB(255, 39, 22, 0),
                          fontSize: 22.0,
                        ),
                        children: [
                          TextSpan(
                            text: widget.curtree.kingdom.title!,
                            style: const TextStyle(
                              decoration: TextDecoration.underline,
                              color: Color.fromARGB(255, 39, 22, 0),
                              fontSize: 22.0,
                            ),
                            recognizer: TapGestureRecognizer()
                              ..onTap = () {
                                setState(
                                  () {
                                    imagePath =
                                        widget.curtree.kingdom.imagePath;
                                    if (imagePath != null) {
                                      isImageExist = true;
                                    } else {
                                      isImageExist = false;
                                    }
                                    description =
                                        widget.curtree.kingdom.description;
                                    if (description != null) {
                                      isDescriptionExist = true;
                                    } else {
                                      isDescriptionExist = false;
                                    }
                                  },
                                );
                              },
                          ),
                        ],
                      ),
                    ),
                  ),
                  TreeNode(
                    content: RichText(
                      text: TextSpan(
                        text: '- Отдел: ',
                        style: const TextStyle(
                          color: Color.fromARGB(255, 39, 22, 0),
                          fontSize: 22.0,
                        ),
                        children: [
                          TextSpan(
                            text: widget.curtree.clade.title!,
                            style: const TextStyle(
                              decoration: TextDecoration.underline,
                              color: Color.fromARGB(255, 39, 22, 0),
                              fontSize: 22.0,
                            ),
                            recognizer: TapGestureRecognizer()
                              ..onTap = () {
                                setState(
                                  () {
                                    imagePath = widget.curtree.clade.imagePath;
                                    if (imagePath != null) {
                                      isImageExist = true;
                                    } else {
                                      isImageExist = false;
                                    }
                                    description =
                                        widget.curtree.clade.description;
                                    if (description != null) {
                                      isDescriptionExist = true;
                                    } else {
                                      isDescriptionExist = false;
                                    }
                                  },
                                );
                              },
                          ),
                        ],
                      ),
                    ),
                  ),
                  TreeNode(
                    content: RichText(
                      text: TextSpan(
                        text: '- Порядок: ',
                        style: const TextStyle(
                          color: Color.fromARGB(255, 39, 22, 0),
                          fontSize: 22.0,
                        ),
                        children: [
                          TextSpan(
                            text: widget.curtree.order.title!,
                            style: const TextStyle(
                              decoration: TextDecoration.underline,
                              color: Color.fromARGB(255, 39, 22, 0),
                              fontSize: 22.0,
                            ),
                            recognizer: TapGestureRecognizer()
                              ..onTap = () {
                                setState(
                                  () {
                                    imagePath = widget.curtree.order.imagePath;
                                    if (imagePath != null) {
                                      isImageExist = true;
                                    } else {
                                      isImageExist = false;
                                    }
                                    description =
                                        widget.curtree.order.description;
                                    if (description != null) {
                                      isDescriptionExist = true;
                                    } else {
                                      isDescriptionExist = false;
                                    }
                                  },
                                );
                              },
                          ),
                        ],
                      ),
                    ),
                  ),
                  TreeNode(
                    content: RichText(
                      text: TextSpan(
                        text: '- Семейство: ',
                        style: const TextStyle(
                          color: Color.fromARGB(255, 39, 22, 0),
                          fontSize: 22.0,
                        ),
                        children: [
                          TextSpan(
                            text: widget.curtree.family.title!,
                            style: const TextStyle(
                              decoration: TextDecoration.underline,
                              color: Color.fromARGB(255, 39, 22, 0),
                              fontSize: 22.0,
                            ),
                            recognizer: TapGestureRecognizer()
                              ..onTap = () {
                                setState(
                                  () {
                                    imagePath = widget.curtree.family.imagePath;
                                    if (imagePath != null) {
                                      isImageExist = true;
                                    } else {
                                      isImageExist = false;
                                    }
                                    description =
                                        widget.curtree.family.description;
                                    if (description != null) {
                                      isDescriptionExist = true;
                                    } else {
                                      isDescriptionExist = false;
                                    }
                                  },
                                );
                              },
                          ),
                        ],
                      ),
                    ),
                  ),
                  TreeNode(
                    content: RichText(
                      text: TextSpan(
                        text: '- Род: ',
                        style: const TextStyle(
                          color: Color.fromARGB(255, 39, 22, 0),
                          fontSize: 22.0,
                        ),
                        children: [
                          TextSpan(
                            text: widget.curtree.species.title!,
                            style: const TextStyle(
                              decoration: TextDecoration.underline,
                              color: Color.fromARGB(255, 39, 22, 0),
                              fontSize: 22.0,
                            ),
                            recognizer: TapGestureRecognizer()
                              ..onTap = () {
                                setState(
                                  () {
                                    imagePath =
                                        widget.curtree.species.imagePath;
                                    if (imagePath != null) {
                                      isImageExist = true;
                                    } else {
                                      isImageExist = false;
                                    }
                                    description =
                                        widget.curtree.species.description;
                                    if (description != null) {
                                      isDescriptionExist = true;
                                    } else {
                                      isDescriptionExist = false;
                                    }
                                  },
                                );
                              },
                          ),
                        ],
                      ),
                    ),
                  ),
                ],
              ),
            ),
            Expanded(
              flex: 3,
              child: Row(
                children: [
                  Expanded(
                    child: Padding(
                      padding: const EdgeInsets.all(8.0),
                      child: Container(
                        decoration: BoxDecoration(
                          color: const Color.fromARGB(255, 95, 141, 78),
                          border: Border.all(
                            width: 2,
                            color: Colors.white10,
                          ),
                          borderRadius: BorderRadius.circular(12),
                        ),
                        child: Column(
                          children: [
                            const Expanded(
                              flex: 1,
                              child: Text(
                                "Описание: ",
                                style: TextStyle(
                                  color: Color.fromARGB(255, 39, 22, 0),
                                  fontSize: 22.0,
                                ),
                              ),
                            ),
                            Expanded(
                              flex: 4,
                              child: isDescriptionExist
                                  ? Text(
                                      description!,
                                      style: const TextStyle(
                                        color: Color.fromARGB(255, 39, 22, 0),
                                        fontSize: 22.0,
                                      ),
                                    )
                                  : const Text(
                                      "У выбранного параметра отсутвует описание",
                                      style: TextStyle(
                                        color: Color.fromARGB(255, 39, 22, 0),
                                        fontSize: 22.0,
                                      ),
                                    ),
                            ),
                          ],
                        ),
                      ),
                    ),
                  ),
                  Expanded(
                    child: Padding(
                      padding: const EdgeInsets.all(8.0),
                      child: Container(
                        decoration: BoxDecoration(
                          color: const Color.fromARGB(255, 95, 141, 78),
                          border: Border.all(
                            width: 2,
                            color: Colors.white10,
                          ),
                          borderRadius: BorderRadius.circular(12),
                        ),
                        child: isImageExist
                            ? Image.network(imagePath!)
                            : const Center(
                                child: Icon(Icons.image),
                              ),
                      ),
                    ),
                  ),
                ],
              ),
            ),
          ],
        ),
      ),
    );
  }
}
