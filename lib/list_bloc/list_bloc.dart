import 'dart:async';

import 'package:bloc/bloc.dart';
import 'package:flutter_lab4/entities/plant_tree.dart';
import 'package:meta/meta.dart';

import '../features/hive_controller.dart';

part 'list_event.dart';
part 'list_state.dart';

class ListBloc extends Bloc<ListEvent, ListState> {
  ListBloc() : super(const ListState()) {
    on<UpdateLinks>(_onUpdateLinks);
    on<DeleteLinks>(_onDeleteLinks);
  }

  FutureOr<void> _onUpdateLinks(
      UpdateLinks event, Emitter<ListState> emit) async {
    final result = await HiveController.instance.getAllItemsFromHive();
    emit(state.copyWith(notes: result));
  }

  FutureOr<void> _onDeleteLinks(
      DeleteLinks event, Emitter<ListState> emit) async {
    await HiveController.instance.deleteItemFromHive(event.num);
  }
}
